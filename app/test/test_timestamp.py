'''Test cases adapted from:
https://github.com/freeCodeCamp/freeCodeCamp/blob/master/curriculum/challenges/english/05-apis-and-microservices/apis-and-microservices-projects/timestamp-microservice.english.md
'''
import json
from datetime import datetime, timedelta, timezone

import pytest
import requests
import azure.functions as func
from app.timestamp.timestamp import main

DATE_FORMAT = '%a, %d %b %Y %H:%M:%S %Z'

def make_timestamp_request(date, url=None):
    '''Return response from API call to date resource.

    If a url is provided makes a live request. Else calls function directly with mock request.'''
    if url:
        req = requests.get(url+date)
        res = func.HttpResponse(req.text, status_code=req.status_code)
    else:
        req = func.HttpRequest(method='GET', url=date, body=None, route_params={'datestring':date})
        res = main(req)
    return res

@pytest.mark.parametrize("date, expected_response", [
    ("2016-12-25", '{"unix":1482624000000,"utc":"Sun, 25 Dec 2016 00:00:00 GMT"}'),
    ("1482624000000", '{"unix":1482624000000,"utc":"Sun, 25 Dec 2016 00:00:00 GMT"}'),
    ("this-is-not-a-date", '{"error":"Invalid Date"}')])
def test_main_timestamp(date, expected_response, deployment_url):
    '''Test requests with datestring parameter.'''
    res = make_timestamp_request(date, url=deployment_url)
    assert res.status_code == 200
    assert json.loads(res.get_body()) == json.loads(expected_response)

def test_main_empty(deployment_url):
    '''Test requests with datestring parameter.'''
    res = make_timestamp_request("", url=deployment_url)
    assert res.status_code == 200
    moment1 = datetime.utcnow().replace(tzinfo=timezone.utc) - timedelta(seconds=1)
    msg = json.loads(res.get_body())
    moment2 = moment1 + timedelta(seconds=10)
    parsed_timestamp = datetime.fromtimestamp(msg['unix'] // 1000, tz=timezone.utc)
    assert moment1 <= parsed_timestamp <= moment2
    parsed_date = datetime.strptime(msg['utc'], DATE_FORMAT).replace(tzinfo=timezone.utc)
    assert moment1 <= parsed_date <= moment2
