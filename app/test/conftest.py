import pytest

def pytest_addoption(parser):
    parser.addoption("--deployment-url", action="store", type=str, default=None, help="URL of the deployed function")

@pytest.fixture
def deployment_url(request):
    return request.config.getoption("--deployment-url")
