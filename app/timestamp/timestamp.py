"""Azure Functions implementation of freeCodeCamp API Project: Timestamp Microservice"""
import logging

from datetime import datetime, timezone, timedelta
import time
import json

import azure.functions as func

GMT = timezone(timedelta(0), "GMT")

def main(req: func.HttpRequest) -> func.HttpResponse:
    '''Return timestamp and formatted date.'''

    logging.info('Timestamp function processed a request.')

    datestring = req.route_params.get('datestring')
    date = None

    if not datestring:
        # get current time if datestring is empty
        date = datetime.utcnow().replace(tzinfo=GMT)
    else:
        # try to parse as ISO-8601 (YYYY-MM-DD)
        try:
            date = datetime(*(time.strptime(datestring, "%Y-%m-%d")[0:3]), tzinfo=GMT)
        except ValueError:
            pass
        # try to parse as a timestamp in milliseconds
        try:
            timestamp = int(datestring)
            date = datetime.fromtimestamp(timestamp / 1000, tz=GMT)
        except ValueError:
            pass
        # return error if time could not be parsed
        if not date:
            return func.HttpResponse(json.dumps({"error": "Invalid Date"}))

    return func.HttpResponse(json.dumps({
        "unix": int(date.timestamp() * 1000),
        "utc": date.strftime("%a, %d %b %Y %H:%M:%S %Z")
    }))
