# Timestamp Microservice

Azure Functions implementation of the Timestamp Microservice project from freeCodeCamp.org.

## Testing

```shell
# run local unittests
python -m pytest app
# run unittests on running function
python -m pytest app --deployment-url <url>/api/timestamp/
```

## CLI Deployment

Assumes you have [Azure CLI](https://github.com/Azure/azure-cli) and [Azure Functions Core Tools](https://github.com/Azure/azure-functions-core-tools) installed.

```shell
# define variables
export AZURE_RESOURCE_GROUP_NAME=<string>
export AZURE_STORAGE_ACCOUNT_NAME=<string>
export AZURE_FUNCTIONS_APP_NAME=<string>
export AZURE_ENABLE_INSIGHTS=<bool>
# deploy app
az group deployment create -g $AZURE_RESOURCE_GROUP_NAME \
--template-file azuredeploy.json \
--parameters \
  appName=$AZURE_FUNCTIONS_APP_NAME \
  storageName=$AZURE_STORAGE_ACCOUNT_NAME \
  enableInsights=$AZURE_ENABLE_INSIGHTS
# publish function
func azure functionapp publish $AZURE_FUNCTIONS_APP_NAME
```
